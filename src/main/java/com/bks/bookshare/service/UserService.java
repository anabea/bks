package com.bks.bookshare.service;

import com.bks.bookshare.dao.UserDAO;
import com.bks.bookshare.entity.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserService {
    private final UserDAO userDAO;

    @Autowired
    public UserService(UserDAO userDAO) {
        this.userDAO = userDAO;
    }

    public User addUser(User user) {
        return userDAO.save(user);
    }

    public Iterable<User> findAllUsers() {
        return userDAO.findAll();
    }

    public void deleteById(Integer id) {
        userDAO.deleteById(id);
    }

    public User updateUser(Integer id, User user) {
        return userDAO.updateUser(id, user);
    }
}
