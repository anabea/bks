package com.bks.bookshare.dao;

import com.bks.bookshare.entity.User;
import org.springframework.data.repository.CrudRepository;

import java.util.Optional;

public interface UserDAO extends CrudRepository<User, Integer> {

    default User updateUser(Integer id, User user) {
        Optional<User> userToUpdate = findById(id);
        if (userToUpdate.isPresent()) {
            User userFound = userToUpdate.get();
            userFound.setFirstName(user.getFirstName());
            userFound.setLastName(user.getLastName());
            userFound.setEmail(user.getEmail());
            userFound.setPhoneNumber(user.getPhoneNumber());
            userFound.setCounty(user.getCounty());
            userFound.setStreet(user.getStreet());
            userFound.setBlock(user.getBlock());
            userFound.setNrApartment(user.getNrApartment());
            userFound.setPostCode(user.getPostCode());

            return save(userFound);
        }
        return null;
    }
}
