package com.bks.bookshare.controller;

import com.bks.bookshare.entity.User;
import com.bks.bookshare.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

@Controller
public class UserController {
    private final UserService userService;

    @Autowired
    public UserController(UserService userService) {
        this.userService = userService;
    }

    public User addUser(User user) {
        return userService.addUser(user);
    }

    public Iterable<User> findAllUsers() {
        return userService.findAllUsers();
    }

    public void deleteById(Integer id) {
        userService.deleteById(id);
    }

    public User updateUser(Integer id, User user) {
        return userService.updateUser(id, user);
    }
}
