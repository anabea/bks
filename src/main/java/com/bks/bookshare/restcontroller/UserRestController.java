package com.bks.bookshare.restcontroller;

import com.bks.bookshare.controller.UserController;
import com.bks.bookshare.entity.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.bind.support.SessionStatus;

import javax.naming.Binding;

@RestController
@RequestMapping("/user")
@SessionAttributes("user")
public class UserRestController {

    private final UserController userController;

    @Autowired
    public UserRestController(UserController userController) {
        this.userController = userController;
    }

    @RequestMapping(method = RequestMethod.GET)
    public String setupForm(Model model) {
        User user = new User();
        model.addAttribute("user", user);
        return "addUser";
    }

    @PostMapping
    public String addUser(@ModelAttribute("user") User user, BindingResult bindingResult, SessionStatus sessionStatus) {
        sessionStatus.setComplete();
        return "redirect:hello";
    }

    @GetMapping(path = "/getAll")
    public @ResponseBody
    Iterable<User> findAllUsers() {
        return userController.findAllUsers();
    }

    @GetMapping(path = "/deleteById")
    public @ResponseBody
    void deleteById(@RequestParam Integer id) {
        userController.deleteById(id);
    }

    @PutMapping(path = "/updateUser/{id}")
    public @ResponseBody
    User updateUser(@PathVariable Integer id, @RequestBody User user) {
        return userController.updateUser(id, user);
    }
}
