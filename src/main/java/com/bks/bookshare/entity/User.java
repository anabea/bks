package com.bks.bookshare.entity;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;

@Entity
@Table(name = "USER")
public class User {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "ID")
    private int id;

    @Column(name = "FIRST_NAME")
    @NotBlank(message = "First name is mandatory")
    private String firstName;

    @Column(name = "LAST_NAME")
    @NotBlank(message = "Last name is mandatory")
    private String lastName;

    @Column(name = "EMAIL")
    @NotBlank(message = "Email is mandatory")
    private String email;

    @Column(name = "PHONE")
    private String phoneNumber;

    @Column(name = "COUNTY")
    @NotBlank(message = "Mandatory")
    private String county;

    @Column(name = "CITY")
    @NotBlank(message = "Mandatory")
    private String city;

    @Column(name = "STREET")
    @NotBlank(message = "Mandatory")
    private String street;

    @Column(name = "BLOCK")
    @NotBlank(message = "Mandatory")
    private String block;

    @Column(name = "APARTMENT")
    @NotBlank(message = "Mandatory")
    private String nrApartment;

    @Column(name = "POST_CODE")
    @NotBlank(message = "Mandatory")
    private long postCode;


    public User() {
    }

    public User(int id, String firstName, String lastName, String email, String phoneNumber, String county, String city, String street, String block, String nrApartment, long postCode) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.phoneNumber = phoneNumber;
        this.county = county;
        this.city = city;
        this.street = street;
        this.block = block;
        this.nrApartment = nrApartment;
        this.postCode = postCode;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getCounty() {
        return county;
    }

    public void setCounty(String county) {
        this.county = county;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getBlock() {
        return block;
    }

    public void setBlock(String block) {
        this.block = block;
    }

    public String getNrApartment() {
        return nrApartment;
    }

    public void setNrApartment(String nrApartment) {
        this.nrApartment = nrApartment;
    }

    public long getPostCode() {
        return postCode;
    }

    public void setPostCode(long postCode) {
        this.postCode = postCode;
    }
}
